package application;
import vehicles.Bicycle; 
 public class BikeStore
 {
 
   public static void main (String args[])
   {
     Bicycle[] bicycles = new Bicycle[4];
 
     bicycles[0]= new Bicycle("Giant Talon",16, 40.00);
     bicycles[1]=new Bicycle ("Specialized Sirrus",16, 38.00);
     bicycles[2]=new Bicycle ("Cannondale Synapse",22, 56.00);
     bicycles[3]=new Bicycle ("Brompton",6, 32.00);

     for (Bicycle bike : bicycles)
     {
       System.out.println(bike.toString());
     }
   }
 }
